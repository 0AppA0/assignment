const playerProfile = {
	name: 'Sylvestor',
	lobbyStatus: 'inviteOnly',
	lobbyType: 'competitive',
	gameMode: 'squadBattles',
	inQueue: false,
	searching: false,
	matchFound: false
}

const lobbySquad = [
	{
		name: 'Sylvestor',
		imageUrl: 'avatar.png',
		partyStatus: 'ready',
		lobbyLeader: true
	},
	{ name: '', partyStatus: 'notReady', lobbyLeader: false }
]

export default {
	playerProfile,
	lobbySquad
}
