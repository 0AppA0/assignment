const mutatePlayerProfile = (context, params) =>
	context.commit('MUTATE_PLAYER_PROFILE', params)

const updateLobbySquad = (context, params) =>
	context.commit('UPDATE_LOBBY_SQUAD', params)

export default {
	mutatePlayerProfile,
	updateLobbySquad
}
