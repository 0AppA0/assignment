const MUTATE_PLAYER_PROFILE = (state, params) => {
	state.playerProfile = {
		...state.playerProfile,
		[params.key]: params.value
	}
}

const UPDATE_LOBBY_SQUAD = (state, params) => {
	state.lobbySqaud = state.lobbySquad.splice(1, 1, params)
}

export default {
	MUTATE_PLAYER_PROFILE,
	UPDATE_LOBBY_SQUAD
}
