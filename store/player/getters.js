const playerProfile = state => state.playerProfile

const lobbySquad = state => state.lobbySquad

export default {
	playerProfile,
	lobbySquad
}
