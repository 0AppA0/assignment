const gameModes = [
	{ label: 'Squad Battles', value: 'squadBattles' },
	{ lable: 'Ultimate Team', value: 'ultimateTeam' },
	{ label: 'Kick Off', value: 'kickOff' },
	{ label: 'Skill Games', value: 'skillGames' },
	{ label: 'Practice Arena', value: 'practiceArena' }
]

const gameType = [
	{ label: 'Competitive', value: 'competitive' },
	{ label: 'Unrated', value: 'unrated' }
]

const lobbyStatus = [
	{ label: 'Invite Only', value: 'inviteOnly' },
	{ label: 'Open', value: 'open' }
]
export default {
	gameModes,
	gameType,
	lobbyStatus
}
