const lobbyStatus = state => state.lobbyStatus

const gameType = state => state.gameType

const gameModes = state => state.gameModes

export default {
	lobbyStatus,
	gameType,
	gameModes
}
