/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
	theme: {
		extend: {
			colors: {
				'gfinity-red': '#E94235',
				'electric-blue': '#293894',
				'dark-gray': '#1B1B1B',
				'lignt-gray': '#6B6B65',
				'concrete': '#CFD1C7',
				'sand': '#DFE0D9',
				'white': '#FFFFFF',
			}
		}
	},
	variants: {},
	plugins: []
}
